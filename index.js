// Quản lý Tuyển Sinh
document
  .getElementById("tinhDiem")
  .addEventListener("click", function tinhDiem() {
    // input
    var diemChuan = document.getElementById("diem-chuan").value * 1;
    var khuVucValue = document.getElementById("khu-vuc").value * 1;
    var doiTuongValue = document.getElementById("doi-tuong").value * 1;
    var diemMonThu1 = document.getElementById("diem-mon-thu-1").value * 1;
    var diemMonThu2 = document.getElementById("diem-mon-thu-2").value * 1;
    var diemMonThu3 = document.getElementById("diem-mon-thu-3").value * 1;
    var inKetQua = document.getElementById("result");
    var ketQua = "";
    // Xử Lý
    var tongdiem =
      khuVucValue + doiTuongValue + diemMonThu1 + diemMonThu2 + diemMonThu3;
    console.log(`🚀 ~ tinhDiem ~ tongdiem:`, tongdiem);
    if (diemChuan == 0) {
      alert("Vui lòng nhập điểm chuẩn");
      return tinhDiem;
    }
    if (tongdiem >= diemChuan) {
      ketQua = `<h3>Tổng Điểm: ${tongdiem}. Bạn đã đậu</h3>`;
    } else {
      ketQua = `<h3>Tổng Điểm: ${tongdiem}. Bạn đã rớt</h3>`;
    }
    // OutPut
    inKetQua.innerHTML = ketQua;
  });

// Tính tiền điện
document
  .getElementById("tinh-tien-dien")
  .addEventListener("click", function tinhTienDien() {
    // input
    var hoTen = document.getElementById("ho-ten").value;
    var soKW = document.getElementById("so-kw").value * 1;
    var tienDien = 0;
    // Xử lý
    var f50KW = 500 * 50;
    var c50KW = 650 * 50;
    var c100KW = 850 * 100;
    var c150KW = 1100 * 150;
    if (soKW == 0) {
      alert("Vui lòng nhập số kw");
      return tinhTienDien;
    }
    if (soKW <= 50) {
      tienDien = 500 * soKW;
    } else if (soKW <= 100) {
      tienDien = f50KW + (soKW - 50) * 650;
    } else if (soKW <= 200) {
      tienDien = f50KW + c50KW + (soKW - 100) * 850;
    } else if (soKW <= 350) {
      tienDien = f50KW + c50KW + c100KW + (soKW - 200) * 1100;
    } else {
      tienDien = f50KW + c50KW + c100KW + c150KW + (soKW - 350) * 1300;
    }
    // Output
    document.getElementById(
      "tienDien"
    ).innerHTML = `<h3>Họ tên: ${hoTen}; Tiền điện: ${tienDien.toLocaleString()} VND</h3>`;
  });

// Tính Thuế Thu Thập Cá Nhân
document
  .getElementById("tinh-tien-thue")
  .addEventListener("click", function tinhTienThue() {
    // Input
    var hoTen1 = document.getElementById("ho-ten1").value;
    var tongThuNhap = document.getElementById("tong-thu-nhap").value * 1;
    var soNguoiPhuThuoc =
      document.getElementById("so-nguoi-phu-thuoc").value * 1;

    // Xử Lý
    var thuNhapChiuThue = tongThuNhap - 4e6 - soNguoiPhuThuoc * 16e5;
    console.log(
      `🚀 ~ tinhTienThue ~ soNguoiPhuThuoc:`,
      soNguoiPhuThuoc * 1.6e5
    );
    var thueCaNhan = 0;
    if (tongThuNhap == 0) {
      alert("Nhập Tổng Thu Thập Cá Nhân");
      return tinhTienThue;
    }
    if (thuNhapChiuThue <= 60e6) {
      thueCaNhan = thuNhapChiuThue * 0.05;
    } else if (thuNhapChiuThue <= 120e6) {
      thueCaNhan = thuNhapChiuThue * 0.1;
    } else if (thuNhapChiuThue <= 210e6) {
      thueCaNhan = thuNhapChiuThue * 0.15;
    } else if (thuNhapChiuThue <= 384e6) {
      thueCaNhan = thuNhapChiuThue * 0.2;
    } else if (thuNhapChiuThue <= 624e6) {
      thueCaNhan = thuNhapChiuThue * 0.25;
    } else if (thuNhapChiuThue <= 960e6) {
      thueCaNhan = thuNhapChiuThue * 0.3;
    } else {
      thueCaNhan = thuNhapChiuThue * 0.35;
    }

    // output
    if (thueCaNhan < 0) {
      document.getElementById(
        "tienThue"
      ).innerHTML = `<h3>Họ tên: ${hoTen1}; Không cần nộp thuế</h3>`;
    } else {
      document.getElementById(
        "tienThue"
      ).innerHTML = `<h3>Họ tên: ${hoTen1}; Tiền Thuế: ${thueCaNhan.toLocaleString()} VND</h3>`;
    }
  });

// Tính Tiền Cáp
document
  .getElementById("loai-kh")
  .addEventListener("change", function loaiKhachHang() {
    var x = document.getElementById("loai-kh").value * 1;
    var selectForm = document.getElementById("form").classList;
    if (x == 2) {
      selectForm.remove("d-none");
    } else {
      selectForm.add("d-none");
    }
  });

document
  .getElementById("tinh-tien-cap")
  .addEventListener("click", function tinhTienCap() {
    // input
    var loaiKH = document.getElementById("loai-kh").value * 1;
    var maKH = document.getElementById("ma-kh").value;
    var soKenhCaoCap = document.getElementById("kenh-cao-cap").value * 1;
    var tienCap = 0;
    var soKetNoi = document.getElementById("so-ket-noi").value * 1;
    var phiDVDoanhNghiep = 0;
    // Xử lý
    if (loaiKH == 0) {
      alert("Vui lòng nhập loại khách hàng");
      return tinhTienCap;
    }
    if (soKetNoi <= 10) {
      phiDVDoanhNghiep = 75;
    } else {
      phiDVDoanhNghiep = (soKetNoi - 10) * 5;
    }

    if (loaiKH == 0) {
      alert("Hãy chọn loại khách hàng");
      return tinhTienCap;
    } else if (loaiKH == 1) {
      tienCap = 4.5 + 20.5 + 7.5 * soKenhCaoCap;
    } else {
      tienCap = 15 + phiDVDoanhNghiep + 50 * soKenhCaoCap;
    }
    console.log(`🚀 ~ tinhTienCap ~ tienCap:`, tienCap);

    // Output
    document.getElementById(
      "tienCap"
    ).innerHTML = `<h3>Mã khách hàng: ${maKH}, Tiền cáp: $${tienCap.toFixed(
      2
    )}</h3>`;
  });
